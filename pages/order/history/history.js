const util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    history:"",
    time:"",
    path: getApp().globalData.path 
  },

  //下拉刷新
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    var that = this;
    const openId = wx.getStorageSync('openId');
    wx.request({
      //发起请求，获取姓名、下单时间、出生时间等信息
      url: getApp().globalData.path + "/selectHostory/selectHistoryOrder?user_id=" + openId,
      dataType: "json",
      success: function (res) {
        var obj = res.data;
        for (var i = 0; i < res.data.length; i++) {

          obj[i].birthDate = util.formatDate(new Date(res.data[i].birthDate), "Y年M月D日")
          obj[i].creatDate = util.formatDate(new Date(res.data[i].creatDate), "Y年M月D日")
        }
        that.setData({
          hostory: obj
        })
      },
      complete: function () {
        // complete
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  },

  toDetails:function(){
    wx.navigateTo({
      url: '../orderDetail/orderDetail',
    })
  },

  onShow:function(){
    var that = this;
    const openId = wx.getStorageSync('openId');
    wx.request({
      //发起请求，获取姓名、下单时间、出生时间等信息
      url: getApp().globalData.path +"/selectHostory/selectHistoryOrder?user_id=" + openId,
      dataType:"json",
      success:function(res){   
        var obj = res.data;
        for (var i = 0; i < res.data.length; i++) {
      
          obj[i].birthDate = util.formatDate(new Date(res.data[i].birthDate), "Y年M月D日")
          obj[i].creatDate = util.formatDate(new Date(res.data[i].creatDate), "Y年M月D日")
        }
        that.setData({
          hostory: obj
        }) 

      }
    })
  }
})