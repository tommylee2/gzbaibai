// pages/order/orderDetail/orderDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    feedbacks:[],
    name:"",
    sex:"",
    birthDate:"",
    time:"",
    createDate:"",
    yearColumn:"",
    monthColumn:"",
    dayColumn:"",
    timeColumn:"",
    id:"",
    path: getApp().globalData.path 
  },
  onLoad:function(options){
    this.setData({
      id: options.id
    });
    const that = this;
    wx.request({
      url: getApp().globalData.path +'/horoscope/detail', 
      data: {
        id: options.id
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },
      success: function (res) {
        that.setData({
          name: options.name,
          sex:options.sex,
          birthDate: options.birthDate,
          time: options.time,
          createDate: options.createDate,
          yearColumn: options.yearColumn,
          monthColumn: options.monthColumn,
          dayColumn: options.dayColumn,
          timeColumn: options.timeColumn,
          feedbacks:res.data.feedbacks,
          
        })
      }
    })
  },

  //下拉刷新
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    const that = this;
    wx.request({
      url: getApp().globalData.path + '/horoscope/detail',
      data: {
        id: that.data.id
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        that.setData({
          feedbacks: res.data.feedbacks
        })
      },
      complete: function () {
        // complete
        wx.hideNavigationBarLoading() //完成停止加载
        wx.stopPullDownRefresh() //停止下拉刷新
      }
    })
  }
})