Page({

  /**
   * 页面的初始数据
   */
  data: {
    end:new Date(),
    date:"",
    birthtime:"",
    sex:"",
    multiArray: ['未知','子时　23:00-0:59', '丑时　1:00-2:59', '寅时　3:00-4:59', '卯时　5:00-6:59', '辰时　7:00-8:59', '巳时　9:00-10:59', '午时　11:00-12:59', '未时　13:00-14:59', '申时　15:00-16:59', '酉时　17:00-18:59', '戌时　19:00-20:59','亥时　21:00-22:59'],
    selectorIndex: 0 ,
    path: getApp().globalData.path ,
    name:''
  },
 

  //出生日期的处理函数
  bindDateChange:function(e){
    
    this.setData({
      date : e.detail.value
    })
    
  },
  //出生时间的处理函数
  bindTimeChange:function(e){
    this.setData({
      birthtime:e.detail.value
    })
  },
  //性别处理函数
  radioChange:function(e){
      this.setData({
        sex:e.detail.value
      })
  },
  //表单提交处理函数
  formSubmit:function(e){
    
    var han = /^[\u4e00-\u9fa5]+$/;
    
    if (!e.detail.value.name || !e.detail.value.birthday){
      
      wx.showModal({
        title: '提示',
        content: '请输入完整的信息',
        showCancel:false,
        success: function (res) {
         return;
        }
      })
    } else if (!han.test(e.detail.value.name)){
      wx.showModal({
        title: '提示',
        content: '姓名必须为汉字',
        showCancel: false,
        success: function (res) {
          return;
        }
      })
    }else{

      wx.checkSession({
        fail: function () {
          //登录态过期
          wx.login({
            success: function (res) {
              if (res.code) {
                //发起网络请求
                wx.request({
                  url: getApp().globalData.path + '/wx_login/getOpenId',
                  data: {
                    code: res.code
                  },
                  success: function (res) {
                    wx.setStorageSync('openId', res.data)
                  }
                })
              } else {
                console.log('获取用户登录态失败！' + res.errMsg)
              }
            }
          }) //重新登录
        }
      })
      this.setData({
        name: null,
        date: null,
        selectorIndex: 0
      })
      var attach = {
        name: e.detail.value.name,
        birthDate: e.detail.value.birthday,
        time: e.detail.value.birthtime,
        sex: e.detail.value.sex,
        openId: wx.getStorageSync('openId')
      }
      wx.request({
        url: getApp().globalData.path +'/wx_pay/prepayment',
        data: {
          openId: wx.getStorageSync('openId'),
          money: 0.01,
          product_name: '测八字',
          attach: attach
        },
        method: 'GET',       //这里必须用get请求
        success: function (res) {
          wx.requestPayment({
            'timeStamp': res.data.timeStamp,
            'nonceStr': res.data.nonce_str,
            'package': res.data.package,
            'signType': 'MD5',
            'paySign': res.data.paySign,
            'success': function (res) {
              wx.showToast({
                title: '下单成功！',
                icon: 'success',
                duration: 2000,
                success: function () {
                  setTimeout(function () {
                    wx.navigateTo({
                      url: '../order/history/history',
                    })
                  }, 2000)

                }
              })

            },
            'fail': function (res) {
              wx.showToast({
                title: '支付失败',
                icon: 'loading',
                duration: 2000
              })
            }
          })
        }
      })
    }
  },



  hostoryreporter:function(){
      wx.navigateTo({
        url: '../order/history/history',
      })
  },
  //当选择器某一列的值改变的时候调用
  bindSelectorPickerChange: function (e) {
   this.setData({
     selectorIndex:e.detail.value
   })
  }

})